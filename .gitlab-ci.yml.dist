image: docker:20.10.16
services:
  - docker:20.10.16-dind

variables:
  DOCKER_REGISTRY_URL: registry.gitlab.com
  DOCKER_TLS_CERTDIR: "/certs"
  ENV:
    value: "dev"
    description: "On wich env the image should be deployed"
  TAG:
    value: "latest"
    description: "Version of the image"

stages:
  - test
  - deploy
  - trigger_deploy_to_terraform  # New stage for triggering the other project

test:
  image: golang:latest
  stage: test
  script:
    - apt-get update -y && apt-get install -y nodejs npm git
    - go mod download
    - npm install
    - go test ./tests/testsUnit/userAPI_test.go

deploy:
  stage: deploy
  script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_TOKEN $DOCKER_REGISTRY_URL
    - docker build -t registry.gitlab.com/sdv-open-course-factory/ocf-core-front/${ENV}-backend:${TAG} .
    - docker push registry.gitlab.com/sdv-open-course-factory/ocf-core-front/${ENV}-backend:${TAG}
  needs:
    - test

# New job to trigger the other project's CI
trigger_deploy_to_terraform:
  image: curlimages/curl
  stage: trigger_deploy_to_terraform
  script:
    - curl -X POST --fail -F token=$CI_TRIGGER_TOKEN -F "ref=main" -F "variables[TAG]=$TAG" https://gitlab.com/api/v4/projects/47370418/trigger/pipeline

  needs:
    - deploy
