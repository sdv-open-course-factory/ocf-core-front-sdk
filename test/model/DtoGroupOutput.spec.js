/*
 * User API
 * This is a server to generate slides.
 *
 * OpenAPI spec version: 1.0
 * Contact: contact@solution-libre.fr
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 3.0.46
 *
 * Do not edit the class manually.
 *
 */
(function(root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD.
    define(['expect.js', '../../src/index'], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like environments that support module.exports, like Node.
    factory(require('expect.js'), require('../../src/index'));
  } else {
    // Browser globals (root is window)
    factory(root.expect, root.UserApi);
  }
}(this, function(expect, UserApi) {
  'use strict';

  var instance;

  describe('(package)', function() {
    describe('DtoGroupOutput', function() {
      beforeEach(function() {
        instance = new UserApi.DtoGroupOutput();
      });

      it('should create an instance of DtoGroupOutput', function() {
        // TODO: update the code to test DtoGroupOutput
        expect(instance).to.be.a(UserApi.DtoGroupOutput);
      });

      it('should have the property groupName (base name: "groupName")', function() {
        // TODO: update the code to test the property groupName
        expect(instance).to.have.property('groupName');
        // expect(instance.groupName).to.be(expectedValueLiteral);
      });

      it('should have the property id (base name: "id")', function() {
        // TODO: update the code to test the property id
        expect(instance).to.have.property('id');
        // expect(instance.id).to.be(expectedValueLiteral);
      });

      it('should have the property organisation (base name: "organisation")', function() {
        // TODO: update the code to test the property organisation
        expect(instance).to.have.property('organisation');
        // expect(instance.organisation).to.be(expectedValueLiteral);
      });

      it('should have the property parentGroupId (base name: "parentGroupId")', function() {
        // TODO: update the code to test the property parentGroupId
        expect(instance).to.have.property('parentGroupId');
        // expect(instance.parentGroupId).to.be(expectedValueLiteral);
      });

    });
  });

}));
