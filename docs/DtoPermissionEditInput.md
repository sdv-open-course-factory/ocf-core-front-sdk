# UserApi.DtoPermissionEditInput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**groupId** | **String** |  | [optional] 
**organisationId** | **String** |  | [optional] 
**roleId** | **String** |  | [optional] 
**userId** | **String** |  | [optional] 
