# UserApi.DtoCreatePermissionInput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**group** | **String** |  | 
**organisation** | **String** |  | 
**role** | **String** |  | 
**user** | **String** |  | 
