# UserApi.RefreshApi

All URIs are relative to *//localhost:8000/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**refreshPost**](RefreshApi.md#refreshPost) | **POST** /refresh | Refresh token

<a name="refreshPost"></a>
# **refreshPost**
> DtoUserTokens refreshPost(body)

Refresh token

Rafraichissement du Refresh Token

### Example
```javascript
import {UserApi} from 'user_api';

let apiInstance = new UserApi.RefreshApi();
let body = new UserApi.DtoUserRefreshTokenInput(); // DtoUserRefreshTokenInput | refresh token

apiInstance.refreshPost(body, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**DtoUserRefreshTokenInput**](DtoUserRefreshTokenInput.md)| refresh token | 

### Return type

[**DtoUserTokens**](DtoUserTokens.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

