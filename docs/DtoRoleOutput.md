# UserApi.DtoRoleOutput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**roleName** | **String** |  | [optional] 
