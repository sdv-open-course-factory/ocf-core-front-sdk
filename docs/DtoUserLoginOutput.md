# UserApi.DtoUserLoginOutput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**email** | **String** |  | [optional] 
**firstname** | **String** |  | [optional] 
**id** | **String** |  | [optional] 
**lastname** | **String** |  | [optional] 
**refreshToken** | **String** |  | [optional] 
**token** | **String** |  | [optional] 
