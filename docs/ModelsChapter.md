# UserApi.ModelsChapter

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**courses** | [**[ModelsCourse]**](ModelsCourse.md) |  | [optional] 
**createdAt** | **String** |  | [optional] 
**deletedAt** | [**GormDeletedAt**](GormDeletedAt.md) |  | [optional] 
**footer** | **String** |  | [optional] 
**id** | **String** |  | [optional] 
**introduction** | **String** |  | [optional] 
**_number** | **Number** |  | [optional] 
**sections** | [**[ModelsSection]**](ModelsSection.md) |  | [optional] 
**title** | **String** |  | [optional] 
**updatedAt** | **String** |  | [optional] 
