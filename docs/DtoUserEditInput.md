# UserApi.DtoUserEditInput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**firstname** | **String** |  | [optional] 
**lastname** | **String** |  | [optional] 
**password** | **String** |  | [optional] 
