# UserApi.DtoUserRefreshTokenInput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**refreshToken** | **String** |  | [optional] 
