# UserApi.ModelsUser

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**createdAt** | **String** |  | [optional] 
**deletedAt** | [**GormDeletedAt**](GormDeletedAt.md) |  | [optional] 
**email** | **String** |  | [optional] 
**firstName** | **String** |  | [optional] 
**id** | **String** |  | [optional] 
**lastName** | **String** |  | [optional] 
**password** | **String** |  | [optional] 
**refreshToken** | **String** |  | [optional] 
**sshKeys** | [**[ModelsSshKey]**](ModelsSshKey.md) |  | [optional] 
**token** | **String** |  | [optional] 
**updatedAt** | **String** |  | [optional] 
