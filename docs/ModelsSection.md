# UserApi.ModelsSection

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**chapter** | [**ModelsChapter**](ModelsChapter.md) |  | [optional] 
**chapterID** | **String** |  | [optional] 
**conclusion** | **String** |  | [optional] 
**createdAt** | **String** |  | [optional] 
**deletedAt** | [**GormDeletedAt**](GormDeletedAt.md) |  | [optional] 
**fileName** | **String** |  | [optional] 
**hiddenPages** | **[Number]** |  | [optional] 
**id** | **String** |  | [optional] 
**intro** | **String** |  | [optional] 
**_number** | **Number** |  | [optional] 
**pages** | [**[ModelsPage]**](ModelsPage.md) |  | [optional] 
**parentChapterTitle** | **String** |  | [optional] 
**title** | **String** |  | [optional] 
**updatedAt** | **String** |  | [optional] 
