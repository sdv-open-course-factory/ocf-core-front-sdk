# UserApi.PermissionsApi

All URIs are relative to *//localhost:8000/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**permissionsGet**](PermissionsApi.md#permissionsGet) | **GET** /permissions | Récupération permissions
[**permissionsIdDelete**](PermissionsApi.md#permissionsIdDelete) | **DELETE** /permissions/{id} | Suppression permission
[**permissionsIdGet**](PermissionsApi.md#permissionsIdGet) | **GET** /permissions/{id} | Récupération permission
[**permissionsIdPut**](PermissionsApi.md#permissionsIdPut) | **PUT** /permissions/{id} | Modification permission
[**permissionsPost**](PermissionsApi.md#permissionsPost) | **POST** /permissions | Création permission

<a name="permissionsGet"></a>
# **permissionsGet**
> [DtoPermissionOutput] permissionsGet(authorization)

Récupération permissions

Récupération de tous les permissions dans la base données

### Example
```javascript
import {UserApi} from 'user_api';

let apiInstance = new UserApi.PermissionsApi();
let authorization = "bearer <Add access token here>"; // String | Insert your access token

apiInstance.permissionsGet(authorization, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Insert your access token | [default to bearer &lt;Add access token here&gt;]

### Return type

[**[DtoPermissionOutput]**](DtoPermissionOutput.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="permissionsIdDelete"></a>
# **permissionsIdDelete**
> &#x27;String&#x27; permissionsIdDelete(id, authorization)

Suppression permission

Suppression d&#x27;un permission dans la base de données

### Example
```javascript
import {UserApi} from 'user_api';

let apiInstance = new UserApi.PermissionsApi();
let id = 56; // Number | ID permission
let authorization = "bearer <Add access token here>"; // String | Insert your access token

apiInstance.permissionsIdDelete(id, authorization, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| ID permission | 
 **authorization** | **String**| Insert your access token | [default to bearer &lt;Add access token here&gt;]

### Return type

**&#x27;String&#x27;**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="permissionsIdGet"></a>
# **permissionsIdGet**
> DtoPermissionOutput permissionsIdGet(id, authorization)

Récupération permission

Récupération des informations de la permission

### Example
```javascript
import {UserApi} from 'user_api';

let apiInstance = new UserApi.PermissionsApi();
let id = 56; // Number | ID permission
let authorization = "bearer <Add access token here>"; // String | Insert your access token

apiInstance.permissionsIdGet(id, authorization, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| ID permission | 
 **authorization** | **String**| Insert your access token | [default to bearer &lt;Add access token here&gt;]

### Return type

[**DtoPermissionOutput**](DtoPermissionOutput.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="permissionsIdPut"></a>
# **permissionsIdPut**
> &#x27;String&#x27; permissionsIdPut(body, id, authorization)

Modification permission

Modification d&#x27;un permission dans la base de données

### Example
```javascript
import {UserApi} from 'user_api';

let apiInstance = new UserApi.PermissionsApi();
let body = new UserApi.DtoPermissionEditInput(); // DtoPermissionEditInput | Permission
let id = 56; // Number | ID permission
let authorization = "bearer <Add access token here>"; // String | Insert your access token

apiInstance.permissionsIdPut(body, id, authorization, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**DtoPermissionEditInput**](DtoPermissionEditInput.md)| Permission | 
 **id** | **Number**| ID permission | 
 **authorization** | **String**| Insert your access token | [default to bearer &lt;Add access token here&gt;]

### Return type

**&#x27;String&#x27;**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="permissionsPost"></a>
# **permissionsPost**
> DtoPermissionOutput permissionsPost(body, authorization)

Création permission

Ajoute une nouvellepermission dans la base de données

### Example
```javascript
import {UserApi} from 'user_api';

let apiInstance = new UserApi.PermissionsApi();
let body = new UserApi.DtoCreatePermissionInput(); // DtoCreatePermissionInput | permission
let authorization = "bearer <Add access token here>"; // String | Insert your access token

apiInstance.permissionsPost(body, authorization, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**DtoCreatePermissionInput**](DtoCreatePermissionInput.md)| permission | 
 **authorization** | **String**| Insert your access token | [default to bearer &lt;Add access token here&gt;]

### Return type

[**DtoPermissionOutput**](DtoPermissionOutput.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

