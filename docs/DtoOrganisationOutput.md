# UserApi.DtoOrganisationOutput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**groups** | [**[ModelsGroup]**](ModelsGroup.md) |  | [optional] 
**id** | **String** |  | [optional] 
**name** | **String** |  | [optional] 
