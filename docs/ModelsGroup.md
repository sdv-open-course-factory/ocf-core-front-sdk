# UserApi.ModelsGroup

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**organisation** | [**ModelsOrganisation**](ModelsOrganisation.md) |  | [optional] 
**createdAt** | **String** |  | [optional] 
**deletedAt** | [**GormDeletedAt**](GormDeletedAt.md) |  | [optional] 
**groupName** | **String** |  | [optional] 
**id** | **String** |  | [optional] 
**organisationID** | **String** |  | [optional] 
**parentGroup** | [**ModelsGroup**](ModelsGroup.md) |  | [optional] 
**parentGroupID** | **String** |  | [optional] 
**updatedAt** | **String** |  | [optional] 
