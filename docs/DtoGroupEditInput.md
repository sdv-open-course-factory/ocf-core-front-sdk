# UserApi.DtoGroupEditInput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**groupName** | **String** |  | [optional] 
**parentGroupId** | **String** |  | [optional] 
