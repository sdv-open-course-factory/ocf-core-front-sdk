# UserApi.DtoGenerateCourseInput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**authorEmail** | **String** |  | 
**format** | **String** |  | 
**name** | **String** |  | 
**theme** | **String** |  | 
