# UserApi.LoginApi

All URIs are relative to *//localhost:8000/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**loginPost**](LoginApi.md#loginPost) | **POST** /login | Connexion d&#x27;un utilisateur

<a name="loginPost"></a>
# **loginPost**
> DtoUserTokens loginPost(body)

Connexion d&#x27;un utilisateur

Connecte un utilisateur avec un identifiant et un mot de passe

### Example
```javascript
import {UserApi} from 'user_api';

let apiInstance = new UserApi.LoginApi();
let body = new UserApi.DtoUserLoginInput(); // DtoUserLoginInput | Identifiant et mot de passe

apiInstance.loginPost(body, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**DtoUserLoginInput**](DtoUserLoginInput.md)| Identifiant et mot de passe | 

### Return type

[**DtoUserTokens**](DtoUserTokens.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

