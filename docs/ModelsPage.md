# UserApi.ModelsPage

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**content** | **[String]** |  | [optional] 
**createdAt** | **String** |  | [optional] 
**deletedAt** | **String** |  | [optional] 
**hide** | **Boolean** |  | [optional] 
**id** | **String** |  | [optional] 
**_number** | **Number** |  | [optional] 
**section** | [**ModelsSection**](ModelsSection.md) |  | [optional] 
**sectionID** | **String** |  | [optional] 
**toc** | **[String]** |  | [optional] 
**updatedAt** | **String** |  | [optional] 
