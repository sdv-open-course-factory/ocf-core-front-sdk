# UserApi.DtoRoleEditInput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**roleName** | **String** |  | [optional] 
