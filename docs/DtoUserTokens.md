# UserApi.DtoUserTokens

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**refreshToken** | **String** |  | [optional] 
**token** | **String** |  | [optional] 
