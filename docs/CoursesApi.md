# UserApi.CoursesApi

All URIs are relative to *//localhost:8000/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**coursesGeneratePost**](CoursesApi.md#coursesGeneratePost) | **POST** /courses/generate | Génération d&#x27;un cours
[**coursesGitPost**](CoursesApi.md#coursesGitPost) | **POST** /courses/git | Création cours à partir d&#x27;un dépôt git
[**coursesIdDelete**](CoursesApi.md#coursesIdDelete) | **DELETE** /courses/{id} | Suppression cours
[**coursesPost**](CoursesApi.md#coursesPost) | **POST** /courses | Création cours

<a name="coursesGeneratePost"></a>
# **coursesGeneratePost**
> DtoGenerateCourseOutput coursesGeneratePost(body, authorization)

Génération d&#x27;un cours

Génération d&#x27;un cours pour un format donné

### Example
```javascript
import {UserApi} from 'user_api';

let apiInstance = new UserApi.CoursesApi();
let body = new UserApi.DtoGenerateCourseInput(); // DtoGenerateCourseInput | cours
let authorization = "bearer <Add access token here>"; // String | Insert your access token

apiInstance.coursesGeneratePost(body, authorization, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**DtoGenerateCourseInput**](DtoGenerateCourseInput.md)| cours | 
 **authorization** | **String**| Insert your access token | [default to bearer &lt;Add access token here&gt;]

### Return type

[**DtoGenerateCourseOutput**](DtoGenerateCourseOutput.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="coursesGitPost"></a>
# **coursesGitPost**
> DtoCreateCourseFromGitOutput coursesGitPost(body, authorization)

Création cours à partir d&#x27;un dépôt git

Ajoute un nouveau cours dans la base de données

### Example
```javascript
import {UserApi} from 'user_api';

let apiInstance = new UserApi.CoursesApi();
let body = new UserApi.DtoCreateCourseFromGitInput(); // DtoCreateCourseFromGitInput | cours
let authorization = "bearer <Add access token here>"; // String | Insert your access token

apiInstance.coursesGitPost(body, authorization, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**DtoCreateCourseFromGitInput**](DtoCreateCourseFromGitInput.md)| cours | 
 **authorization** | **String**| Insert your access token | [default to bearer &lt;Add access token here&gt;]

### Return type

[**DtoCreateCourseFromGitOutput**](DtoCreateCourseFromGitOutput.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="coursesIdDelete"></a>
# **coursesIdDelete**
> &#x27;String&#x27; coursesIdDelete(id, authorization)

Suppression cours

Suppression d&#x27;un cours dans la base de données

### Example
```javascript
import {UserApi} from 'user_api';

let apiInstance = new UserApi.CoursesApi();
let id = "id_example"; // String | ID cours
let authorization = "bearer <Add access token here>"; // String | Insert your access token

apiInstance.coursesIdDelete(id, authorization, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| ID cours | 
 **authorization** | **String**| Insert your access token | [default to bearer &lt;Add access token here&gt;]

### Return type

**&#x27;String&#x27;**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="coursesPost"></a>
# **coursesPost**
> DtoCreateCourseOutput coursesPost(body)

Création cours

Ajoute un nouveau cours dans la base de données

### Example
```javascript
import {UserApi} from 'user_api';

let apiInstance = new UserApi.CoursesApi();
let body = new UserApi.DtoCreateCourseInput(); // DtoCreateCourseInput | cours

apiInstance.coursesPost(body, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**DtoCreateCourseInput**](DtoCreateCourseInput.md)| cours | 

### Return type

[**DtoCreateCourseOutput**](DtoCreateCourseOutput.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

