# UserApi.UsersApi

All URIs are relative to *//localhost:8000/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**usersGet**](UsersApi.md#usersGet) | **GET** /users | Récupération utilisateurs
[**usersIdDelete**](UsersApi.md#usersIdDelete) | **DELETE** /users/{id} | Suppression utilisateur
[**usersIdGet**](UsersApi.md#usersIdGet) | **GET** /users/{id} | Récupération utilisateur
[**usersIdPut**](UsersApi.md#usersIdPut) | **PUT** /users/{id} | Modification utilisateur (Admin)
[**usersPatch**](UsersApi.md#usersPatch) | **PATCH** /users | Modification utilisateur
[**usersPost**](UsersApi.md#usersPost) | **POST** /users | Création utilisateur
[**usersSshkeyPost**](UsersApi.md#usersSshkeyPost) | **POST** /users/sshkey | Ajout d&#x27;une clé SSH à l&#x27;tilisateur courant

<a name="usersGet"></a>
# **usersGet**
> [DtoUserOutput] usersGet(authorization)

Récupération utilisateurs

Récupération de tous les utilisateurs dans la base données

### Example
```javascript
import {UserApi} from 'user_api';

let apiInstance = new UserApi.UsersApi();
let authorization = "bearer <Add access token here>"; // String | Insert your access token

apiInstance.usersGet(authorization, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Insert your access token | [default to bearer &lt;Add access token here&gt;]

### Return type

[**[DtoUserOutput]**](DtoUserOutput.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="usersIdDelete"></a>
# **usersIdDelete**
> &#x27;String&#x27; usersIdDelete(id, authorization)

Suppression utilisateur

Suppression d&#x27;un utilisateur dans la base de données

### Example
```javascript
import {UserApi} from 'user_api';

let apiInstance = new UserApi.UsersApi();
let id = "id_example"; // String | ID utilisateur
let authorization = "bearer <Add access token here>"; // String | Insert your access token

apiInstance.usersIdDelete(id, authorization, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| ID utilisateur | 
 **authorization** | **String**| Insert your access token | [default to bearer &lt;Add access token here&gt;]

### Return type

**&#x27;String&#x27;**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="usersIdGet"></a>
# **usersIdGet**
> DtoUserOutput usersIdGet(id, authorization)

Récupération utilisateur

Récupération des informations de l&#x27;utilisateur

### Example
```javascript
import {UserApi} from 'user_api';

let apiInstance = new UserApi.UsersApi();
let id = "id_example"; // String | ID utilisateur
let authorization = "bearer <Add access token here>"; // String | Insert your access token

apiInstance.usersIdGet(id, authorization, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| ID utilisateur | 
 **authorization** | **String**| Insert your access token | [default to bearer &lt;Add access token here&gt;]

### Return type

[**DtoUserOutput**](DtoUserOutput.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="usersIdPut"></a>
# **usersIdPut**
> &#x27;String&#x27; usersIdPut(body, id, authorization)

Modification utilisateur (Admin)

Modification d&#x27;un utilisateur dans la base de données par un administrateur

### Example
```javascript
import {UserApi} from 'user_api';

let apiInstance = new UserApi.UsersApi();
let body = new UserApi.DtoUserEditInput(); // DtoUserEditInput | Utilisateur
let id = 56; // Number | ID utilisateur
let authorization = "bearer <Add access token here>"; // String | Insert your access token

apiInstance.usersIdPut(body, id, authorization, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**DtoUserEditInput**](DtoUserEditInput.md)| Utilisateur | 
 **id** | **Number**| ID utilisateur | 
 **authorization** | **String**| Insert your access token | [default to bearer &lt;Add access token here&gt;]

### Return type

**&#x27;String&#x27;**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="usersPatch"></a>
# **usersPatch**
> &#x27;String&#x27; usersPatch(body, authorization)

Modification utilisateur

Modification des informations de l&#x27;utilisateur par lui même dans la base de données

### Example
```javascript
import {UserApi} from 'user_api';

let apiInstance = new UserApi.UsersApi();
let body = new UserApi.DtoUserEditInput(); // DtoUserEditInput | Utilisateur
let authorization = "bearer <Add access token here>"; // String | Insert your access token

apiInstance.usersPatch(body, authorization, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**DtoUserEditInput**](DtoUserEditInput.md)| Utilisateur | 
 **authorization** | **String**| Insert your access token | [default to bearer &lt;Add access token here&gt;]

### Return type

**&#x27;String&#x27;**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="usersPost"></a>
# **usersPost**
> DtoUserLoginOutput usersPost(body)

Création utilisateur

Ajoute un nouvel utilisateur dans la base de données

### Example
```javascript
import {UserApi} from 'user_api';

let apiInstance = new UserApi.UsersApi();
let body = new UserApi.DtoCreateUserInput(); // DtoCreateUserInput | utilisateur

apiInstance.usersPost(body, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**DtoCreateUserInput**](DtoCreateUserInput.md)| utilisateur | 

### Return type

[**DtoUserLoginOutput**](DtoUserLoginOutput.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="usersSshkeyPost"></a>
# **usersSshkeyPost**
> &#x27;String&#x27; usersSshkeyPost(body, authorization)

Ajout d&#x27;une clé SSH à l&#x27;tilisateur courant

Ajout d&#x27;une clé SSH à l&#x27;tilisateur courant

### Example
```javascript
import {UserApi} from 'user_api';

let apiInstance = new UserApi.UsersApi();
let body = new UserApi.DtoCreateSshKeyInput(); // DtoCreateSshKeyInput | Clé SSH
let authorization = "bearer <Add access token here>"; // String | Insert your access token

apiInstance.usersSshkeyPost(body, authorization, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**DtoCreateSshKeyInput**](DtoCreateSshKeyInput.md)| Clé SSH | 
 **authorization** | **String**| Insert your access token | [default to bearer &lt;Add access token here&gt;]

### Return type

**&#x27;String&#x27;**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

