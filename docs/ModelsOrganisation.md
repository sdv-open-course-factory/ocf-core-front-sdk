# UserApi.ModelsOrganisation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**createdAt** | **String** |  | [optional] 
**deletedAt** | [**GormDeletedAt**](GormDeletedAt.md) |  | [optional] 
**groups** | [**[ModelsGroup]**](ModelsGroup.md) |  | [optional] 
**id** | **String** |  | [optional] 
**organisationName** | **String** |  | [optional] 
**updatedAt** | **String** |  | [optional] 
