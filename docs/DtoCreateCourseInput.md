# UserApi.DtoCreateCourseInput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**authorEmail** | **String** |  | 
**category** | **String** |  | 
**chapters** | [**[ModelsChapter]**](ModelsChapter.md) |  | [optional] 
**courseIDStr** | **String** |  | 
**description** | **String** |  | [optional] 
**footer** | **String** |  | 
**format** | **Number** |  | 
**header** | **String** |  | 
**learningObjectives** | **String** |  | [optional] 
**logo** | **String** |  | [optional] 
**name** | **String** |  | 
**prelude** | **String** |  | 
**schedule** | **String** |  | 
**subtitle** | **String** |  | [optional] 
**theme** | **String** |  | 
**title** | **String** |  | 
**version** | **String** |  | [optional] 
