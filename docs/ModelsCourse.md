# UserApi.ModelsCourse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**category** | **String** |  | [optional] 
**chapters** | [**[ModelsChapter]**](ModelsChapter.md) |  | [optional] 
**courseIDStr** | **String** |  | [optional] 
**createdAt** | **String** |  | [optional] 
**deletedAt** | [**GormDeletedAt**](GormDeletedAt.md) |  | [optional] 
**description** | **String** |  | [optional] 
**footer** | **String** |  | [optional] 
**format** | [**ModelsFormat**](ModelsFormat.md) |  | [optional] 
**header** | **String** |  | [optional] 
**id** | **String** |  | [optional] 
**learningObjectives** | **String** |  | [optional] 
**logo** | **String** |  | [optional] 
**name** | **String** |  | [optional] 
**owner** | [**ModelsUser**](ModelsUser.md) |  | [optional] 
**ownerID** | **String** |  | [optional] 
**prelude** | **String** |  | [optional] 
**schedule** | **String** |  | [optional] 
**subtitle** | **String** |  | [optional] 
**theme** | **String** |  | [optional] 
**title** | **String** |  | [optional] 
**updatedAt** | **String** |  | [optional] 
**url** | **String** |  | [optional] 
**version** | **String** |  | [optional] 
