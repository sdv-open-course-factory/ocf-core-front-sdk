# UserApi.DtoUserOutput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**email** | **String** |  | [optional] 
**firstname** | **String** |  | [optional] 
**id** | **String** |  | [optional] 
**lastname** | **String** |  | [optional] 
