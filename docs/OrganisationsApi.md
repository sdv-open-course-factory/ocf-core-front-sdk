# UserApi.OrganisationsApi

All URIs are relative to *//localhost:8000/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**organisationsGet**](OrganisationsApi.md#organisationsGet) | **GET** /organisations | Récupération organisations
[**organisationsIdDelete**](OrganisationsApi.md#organisationsIdDelete) | **DELETE** /organisations/{id} | Suppression organisation
[**organisationsIdGet**](OrganisationsApi.md#organisationsIdGet) | **GET** /organisations/{id} | Récupération organisation
[**organisationsIdPut**](OrganisationsApi.md#organisationsIdPut) | **PUT** /organisations/{id} | Modification organisation (Admin)
[**organisationsPatch**](OrganisationsApi.md#organisationsPatch) | **PATCH** /organisations | Modification organisation
[**organisationsPost**](OrganisationsApi.md#organisationsPost) | **POST** /organisations | Création organisation

<a name="organisationsGet"></a>
# **organisationsGet**
> [DtoOrganisationOutput] organisationsGet(authorization)

Récupération organisations

Récupération de toutes les organisations dans la base données

### Example
```javascript
import {UserApi} from 'user_api';

let apiInstance = new UserApi.OrganisationsApi();
let authorization = "bearer <Add access token here>"; // String | Insert your access token

apiInstance.organisationsGet(authorization, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Insert your access token | [default to bearer &lt;Add access token here&gt;]

### Return type

[**[DtoOrganisationOutput]**](DtoOrganisationOutput.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="organisationsIdDelete"></a>
# **organisationsIdDelete**
> &#x27;String&#x27; organisationsIdDelete(id, authorization)

Suppression organisation

Suppression d&#x27;une organisation dans la base de données

### Example
```javascript
import {UserApi} from 'user_api';

let apiInstance = new UserApi.OrganisationsApi();
let id = "id_example"; // String | ID organisation
let authorization = "bearer <Add access token here>"; // String | Insert your access token

apiInstance.organisationsIdDelete(id, authorization, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| ID organisation | 
 **authorization** | **String**| Insert your access token | [default to bearer &lt;Add access token here&gt;]

### Return type

**&#x27;String&#x27;**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="organisationsIdGet"></a>
# **organisationsIdGet**
> DtoOrganisationOutput organisationsIdGet(id, authorization)

Récupération organisation

Récupération des informations de l&#x27;organisation

### Example
```javascript
import {UserApi} from 'user_api';

let apiInstance = new UserApi.OrganisationsApi();
let id = "id_example"; // String | ID organisation
let authorization = "bearer <Add access token here>"; // String | Insert your access token

apiInstance.organisationsIdGet(id, authorization, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| ID organisation | 
 **authorization** | **String**| Insert your access token | [default to bearer &lt;Add access token here&gt;]

### Return type

[**DtoOrganisationOutput**](DtoOrganisationOutput.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="organisationsIdPut"></a>
# **organisationsIdPut**
> &#x27;String&#x27; organisationsIdPut(body, id, authorization)

Modification organisation (Admin)

Modification d&#x27;une organisation dans la base de données par un administrateur

### Example
```javascript
import {UserApi} from 'user_api';

let apiInstance = new UserApi.OrganisationsApi();
let body = new UserApi.DtoOrganisationEditInput(); // DtoOrganisationEditInput | Utilisateur
let id = 56; // Number | ID organisation
let authorization = "bearer <Add access token here>"; // String | Insert your access token

apiInstance.organisationsIdPut(body, id, authorization, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**DtoOrganisationEditInput**](DtoOrganisationEditInput.md)| Utilisateur | 
 **id** | **Number**| ID organisation | 
 **authorization** | **String**| Insert your access token | [default to bearer &lt;Add access token here&gt;]

### Return type

**&#x27;String&#x27;**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="organisationsPatch"></a>
# **organisationsPatch**
> &#x27;String&#x27; organisationsPatch(body, authorization)

Modification organisation

Modification des informations de l&#x27;organisation par lui même dans la base de données

### Example
```javascript
import {UserApi} from 'user_api';

let apiInstance = new UserApi.OrganisationsApi();
let body = new UserApi.DtoOrganisationEditInput(); // DtoOrganisationEditInput | Utilisateur
let authorization = "bearer <Add access token here>"; // String | Insert your access token

apiInstance.organisationsPatch(body, authorization, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**DtoOrganisationEditInput**](DtoOrganisationEditInput.md)| Utilisateur | 
 **authorization** | **String**| Insert your access token | [default to bearer &lt;Add access token here&gt;]

### Return type

**&#x27;String&#x27;**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="organisationsPost"></a>
# **organisationsPost**
> DtoOrganisationOutput organisationsPost(body, authorization)

Création organisation

Ajoute une nouvelle organisation dans la base de données

### Example
```javascript
import {UserApi} from 'user_api';

let apiInstance = new UserApi.OrganisationsApi();
let body = new UserApi.DtoCreateOrganisationInput(); // DtoCreateOrganisationInput | organisation
let authorization = "bearer <Add access token here>"; // String | Insert your access token

apiInstance.organisationsPost(body, authorization, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**DtoCreateOrganisationInput**](DtoCreateOrganisationInput.md)| organisation | 
 **authorization** | **String**| Insert your access token | [default to bearer &lt;Add access token here&gt;]

### Return type

[**DtoOrganisationOutput**](DtoOrganisationOutput.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

