# UserApi.ModelsSshKey

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**createdAt** | **String** |  | [optional] 
**deletedAt** | [**GormDeletedAt**](GormDeletedAt.md) |  | [optional] 
**id** | **String** |  | [optional] 
**name** | **String** |  | [optional] 
**privateKey** | **String** |  | [optional] 
**updatedAt** | **String** |  | [optional] 
**userID** | **String** |  | [optional] 
