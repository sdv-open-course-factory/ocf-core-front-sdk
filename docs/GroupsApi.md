# UserApi.GroupsApi

All URIs are relative to *//localhost:8000/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**groupsGet**](GroupsApi.md#groupsGet) | **GET** /groups | Récupération groupes
[**groupsIdDelete**](GroupsApi.md#groupsIdDelete) | **DELETE** /groups/{id} | Suppression groupe
[**groupsIdGet**](GroupsApi.md#groupsIdGet) | **GET** /groups/{id} | Récupération groupe
[**groupsIdPut**](GroupsApi.md#groupsIdPut) | **PUT** /groups/{id} | Modification groupe
[**groupsPost**](GroupsApi.md#groupsPost) | **POST** /groups | Création groupe

<a name="groupsGet"></a>
# **groupsGet**
> [DtoGroupOutput] groupsGet(authorization)

Récupération groupes

Récupération de tous les groupes dans la base données

### Example
```javascript
import {UserApi} from 'user_api';

let apiInstance = new UserApi.GroupsApi();
let authorization = "bearer <Add access token here>"; // String | Insert your access token

apiInstance.groupsGet(authorization, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Insert your access token | [default to bearer &lt;Add access token here&gt;]

### Return type

[**[DtoGroupOutput]**](DtoGroupOutput.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="groupsIdDelete"></a>
# **groupsIdDelete**
> &#x27;String&#x27; groupsIdDelete(id, authorization)

Suppression groupe

Suppression d&#x27;un groupe dans la base de données

### Example
```javascript
import {UserApi} from 'user_api';

let apiInstance = new UserApi.GroupsApi();
let id = 56; // Number | ID group
let authorization = "bearer <Add access token here>"; // String | Insert your access token

apiInstance.groupsIdDelete(id, authorization, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| ID group | 
 **authorization** | **String**| Insert your access token | [default to bearer &lt;Add access token here&gt;]

### Return type

**&#x27;String&#x27;**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="groupsIdGet"></a>
# **groupsIdGet**
> DtoGroupOutput groupsIdGet(id, authorization)

Récupération groupe

Récupération des informations du groupe

### Example
```javascript
import {UserApi} from 'user_api';

let apiInstance = new UserApi.GroupsApi();
let id = 56; // Number | ID group
let authorization = "bearer <Add access token here>"; // String | Insert your access token

apiInstance.groupsIdGet(id, authorization, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| ID group | 
 **authorization** | **String**| Insert your access token | [default to bearer &lt;Add access token here&gt;]

### Return type

[**DtoGroupOutput**](DtoGroupOutput.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="groupsIdPut"></a>
# **groupsIdPut**
> &#x27;String&#x27; groupsIdPut(body, id, authorization)

Modification groupe

Modification d&#x27;un groupe dans la base de données

### Example
```javascript
import {UserApi} from 'user_api';

let apiInstance = new UserApi.GroupsApi();
let body = new UserApi.DtoGroupEditInput(); // DtoGroupEditInput | Group
let id = 56; // Number | ID group
let authorization = "bearer <Add access token here>"; // String | Insert your access token

apiInstance.groupsIdPut(body, id, authorization, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**DtoGroupEditInput**](DtoGroupEditInput.md)| Group | 
 **id** | **Number**| ID group | 
 **authorization** | **String**| Insert your access token | [default to bearer &lt;Add access token here&gt;]

### Return type

**&#x27;String&#x27;**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="groupsPost"></a>
# **groupsPost**
> DtoGroupOutput groupsPost(body, authorization)

Création groupe

Ajoute un nouveau groupe dans la base de données

### Example
```javascript
import {UserApi} from 'user_api';

let apiInstance = new UserApi.GroupsApi();
let body = new UserApi.DtoCreateGroupInput(); // DtoCreateGroupInput | group
let authorization = "bearer <Add access token here>"; // String | Insert your access token

apiInstance.groupsPost(body, authorization, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**DtoCreateGroupInput**](DtoCreateGroupInput.md)| group | 
 **authorization** | **String**| Insert your access token | [default to bearer &lt;Add access token here&gt;]

### Return type

[**DtoGroupOutput**](DtoGroupOutput.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

