# UserApi.DtoOrganisationEditInput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**groups** | [**[ModelsGroup]**](ModelsGroup.md) |  | [optional] 
**name** | **String** |  | [optional] 
