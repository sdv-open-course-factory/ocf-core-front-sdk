# UserApi.DtoPermissionOutput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**groupId** | **String** |  | [optional] 
**id** | **String** |  | [optional] 
**organisationId** | **String** |  | [optional] 
**roleId** | **String** |  | [optional] 
**userId** | **String** |  | [optional] 
