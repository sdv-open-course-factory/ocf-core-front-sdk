# UserApi.SoliFormationsSrcAuthErrorsAPIError

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**errorCode** | **Number** |  | [optional] 
**errorMessage** | **String** |  | [optional] 
