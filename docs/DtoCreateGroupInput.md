# UserApi.DtoCreateGroupInput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**groupName** | **String** |  | 
**parentGroup** | **String** |  | [optional] 
