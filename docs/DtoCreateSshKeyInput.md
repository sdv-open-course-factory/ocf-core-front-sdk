# UserApi.DtoCreateSshKeyInput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **String** |  | 
**privateKey** | **String** |  | 
**userId** | **String** |  | [optional] 
