# UserApi.RolesApi

All URIs are relative to *//localhost:8000/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**rolesGet**](RolesApi.md#rolesGet) | **GET** /roles | Récupération roles
[**rolesIdDelete**](RolesApi.md#rolesIdDelete) | **DELETE** /roles/{id} | Suppression role
[**rolesIdGet**](RolesApi.md#rolesIdGet) | **GET** /roles/{id} | Récupération role
[**rolesIdPut**](RolesApi.md#rolesIdPut) | **PUT** /roles/{id} | Modification role
[**rolesPost**](RolesApi.md#rolesPost) | **POST** /roles | Création role

<a name="rolesGet"></a>
# **rolesGet**
> [DtoRoleOutput] rolesGet(authorization)

Récupération roles

Récupération de tous les roles dans la base données

### Example
```javascript
import {UserApi} from 'user_api';

let apiInstance = new UserApi.RolesApi();
let authorization = "bearer <Add access token here>"; // String | Insert your access token

apiInstance.rolesGet(authorization, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **authorization** | **String**| Insert your access token | [default to bearer &lt;Add access token here&gt;]

### Return type

[**[DtoRoleOutput]**](DtoRoleOutput.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="rolesIdDelete"></a>
# **rolesIdDelete**
> &#x27;String&#x27; rolesIdDelete(id, authorization)

Suppression role

Suppression d&#x27;un role dans la base de données

### Example
```javascript
import {UserApi} from 'user_api';

let apiInstance = new UserApi.RolesApi();
let id = 56; // Number | ID role
let authorization = "bearer <Add access token here>"; // String | Insert your access token

apiInstance.rolesIdDelete(id, authorization, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| ID role | 
 **authorization** | **String**| Insert your access token | [default to bearer &lt;Add access token here&gt;]

### Return type

**&#x27;String&#x27;**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="rolesIdGet"></a>
# **rolesIdGet**
> DtoRoleOutput rolesIdGet(id, authorization)

Récupération role

Récupération des informations de l&#x27;role

### Example
```javascript
import {UserApi} from 'user_api';

let apiInstance = new UserApi.RolesApi();
let id = 56; // Number | ID role
let authorization = "bearer <Add access token here>"; // String | Insert your access token

apiInstance.rolesIdGet(id, authorization, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **Number**| ID role | 
 **authorization** | **String**| Insert your access token | [default to bearer &lt;Add access token here&gt;]

### Return type

[**DtoRoleOutput**](DtoRoleOutput.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="rolesIdPut"></a>
# **rolesIdPut**
> &#x27;String&#x27; rolesIdPut(body, id, authorization)

Modification role

Modification d&#x27;un role dans la base de données

### Example
```javascript
import {UserApi} from 'user_api';

let apiInstance = new UserApi.RolesApi();
let body = new UserApi.DtoRoleEditInput(); // DtoRoleEditInput | Role
let id = 56; // Number | ID role
let authorization = "bearer <Add access token here>"; // String | Insert your access token

apiInstance.rolesIdPut(body, id, authorization, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**DtoRoleEditInput**](DtoRoleEditInput.md)| Role | 
 **id** | **Number**| ID role | 
 **authorization** | **String**| Insert your access token | [default to bearer &lt;Add access token here&gt;]

### Return type

**&#x27;String&#x27;**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="rolesPost"></a>
# **rolesPost**
> DtoRoleOutput rolesPost(body, authorization)

Création role

Ajoute un nouveau role dans la base de données

### Example
```javascript
import {UserApi} from 'user_api';

let apiInstance = new UserApi.RolesApi();
let body = new UserApi.DtoCreateRoleInput(); // DtoCreateRoleInput | role
let authorization = "bearer <Add access token here>"; // String | Insert your access token

apiInstance.rolesPost(body, authorization, (error, data, response) => {
  if (error) {
    console.error(error);
  } else {
    console.log('API called successfully. Returned data: ' + data);
  }
});
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**DtoCreateRoleInput**](DtoCreateRoleInput.md)| role | 
 **authorization** | **String**| Insert your access token | [default to bearer &lt;Add access token here&gt;]

### Return type

[**DtoRoleOutput**](DtoRoleOutput.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

