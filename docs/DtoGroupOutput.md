# UserApi.DtoGroupOutput

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**groupName** | **String** |  | [optional] 
**id** | **String** |  | [optional] 
**organisation** | **String** |  | [optional] 
**parentGroupId** | **String** |  | [optional] 
