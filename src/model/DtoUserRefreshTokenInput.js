/*
 * User API
 * This is a server to generate slides.
 *
 * OpenAPI spec version: 1.0
 * Contact: contact@solution-libre.fr
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 3.0.46
 *
 * Do not edit the class manually.
 *
 */
import {ApiClient} from '../ApiClient';

/**
 * The DtoUserRefreshTokenInput model module.
 * @module model/DtoUserRefreshTokenInput
 * @version 1.0
 */
export class DtoUserRefreshTokenInput {
  /**
   * Constructs a new <code>DtoUserRefreshTokenInput</code>.
   * @alias module:model/DtoUserRefreshTokenInput
   * @class
   */
  constructor() {
  }

  /**
   * Constructs a <code>DtoUserRefreshTokenInput</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/DtoUserRefreshTokenInput} obj Optional instance to populate.
   * @return {module:model/DtoUserRefreshTokenInput} The populated <code>DtoUserRefreshTokenInput</code> instance.
   */
  static constructFromObject(data, obj) {
    if (data) {
      obj = obj || new DtoUserRefreshTokenInput();
      if (data.hasOwnProperty('refresh_token'))
        obj.refreshToken = ApiClient.convertToType(data['refresh_token'], 'String');
    }
    return obj;
  }
}

/**
 * @member {String} refreshToken
 */
DtoUserRefreshTokenInput.prototype.refreshToken = undefined;

