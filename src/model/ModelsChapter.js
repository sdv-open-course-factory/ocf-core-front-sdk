/*
 * User API
 * This is a server to generate slides.
 *
 * OpenAPI spec version: 1.0
 * Contact: contact@solution-libre.fr
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 3.0.46
 *
 * Do not edit the class manually.
 *
 */
import {ApiClient} from '../ApiClient';
import {GormDeletedAt} from './GormDeletedAt';
import {ModelsCourse} from './ModelsCourse';
import {ModelsSection} from './ModelsSection';

/**
 * The ModelsChapter model module.
 * @module model/ModelsChapter
 * @version 1.0
 */
export class ModelsChapter {
  /**
   * Constructs a new <code>ModelsChapter</code>.
   * @alias module:model/ModelsChapter
   * @class
   */
  constructor() {
  }

  /**
   * Constructs a <code>ModelsChapter</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/ModelsChapter} obj Optional instance to populate.
   * @return {module:model/ModelsChapter} The populated <code>ModelsChapter</code> instance.
   */
  static constructFromObject(data, obj) {
    if (data) {
      obj = obj || new ModelsChapter();
      if (data.hasOwnProperty('courses'))
        obj.courses = ApiClient.convertToType(data['courses'], [ModelsCourse]);
      if (data.hasOwnProperty('createdAt'))
        obj.createdAt = ApiClient.convertToType(data['createdAt'], 'String');
      if (data.hasOwnProperty('deletedAt'))
        obj.deletedAt = GormDeletedAt.constructFromObject(data['deletedAt']);
      if (data.hasOwnProperty('footer'))
        obj.footer = ApiClient.convertToType(data['footer'], 'String');
      if (data.hasOwnProperty('id'))
        obj.id = ApiClient.convertToType(data['id'], 'String');
      if (data.hasOwnProperty('introduction'))
        obj.introduction = ApiClient.convertToType(data['introduction'], 'String');
      if (data.hasOwnProperty('number'))
        obj._number = ApiClient.convertToType(data['number'], 'Number');
      if (data.hasOwnProperty('sections'))
        obj.sections = ApiClient.convertToType(data['sections'], [ModelsSection]);
      if (data.hasOwnProperty('title'))
        obj.title = ApiClient.convertToType(data['title'], 'String');
      if (data.hasOwnProperty('updatedAt'))
        obj.updatedAt = ApiClient.convertToType(data['updatedAt'], 'String');
    }
    return obj;
  }
}

/**
 * @member {Array.<module:model/ModelsCourse>} courses
 */
ModelsChapter.prototype.courses = undefined;

/**
 * @member {String} createdAt
 */
ModelsChapter.prototype.createdAt = undefined;

/**
 * @member {module:model/GormDeletedAt} deletedAt
 */
ModelsChapter.prototype.deletedAt = undefined;

/**
 * @member {String} footer
 */
ModelsChapter.prototype.footer = undefined;

/**
 * @member {String} id
 */
ModelsChapter.prototype.id = undefined;

/**
 * @member {String} introduction
 */
ModelsChapter.prototype.introduction = undefined;

/**
 * @member {Number} _number
 */
ModelsChapter.prototype._number = undefined;

/**
 * @member {Array.<module:model/ModelsSection>} sections
 */
ModelsChapter.prototype.sections = undefined;

/**
 * @member {String} title
 */
ModelsChapter.prototype.title = undefined;

/**
 * @member {String} updatedAt
 */
ModelsChapter.prototype.updatedAt = undefined;

