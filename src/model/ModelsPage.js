/*
 * User API
 * This is a server to generate slides.
 *
 * OpenAPI spec version: 1.0
 * Contact: contact@solution-libre.fr
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 3.0.46
 *
 * Do not edit the class manually.
 *
 */
import {ApiClient} from '../ApiClient';
import {ModelsSection} from './ModelsSection';

/**
 * The ModelsPage model module.
 * @module model/ModelsPage
 * @version 1.0
 */
export class ModelsPage {
  /**
   * Constructs a new <code>ModelsPage</code>.
   * @alias module:model/ModelsPage
   * @class
   */
  constructor() {
  }

  /**
   * Constructs a <code>ModelsPage</code> from a plain JavaScript object, optionally creating a new instance.
   * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
   * @param {Object} data The plain JavaScript object bearing properties of interest.
   * @param {module:model/ModelsPage} obj Optional instance to populate.
   * @return {module:model/ModelsPage} The populated <code>ModelsPage</code> instance.
   */
  static constructFromObject(data, obj) {
    if (data) {
      obj = obj || new ModelsPage();
      if (data.hasOwnProperty('content'))
        obj.content = ApiClient.convertToType(data['content'], ['String']);
      if (data.hasOwnProperty('createdAt'))
        obj.createdAt = ApiClient.convertToType(data['createdAt'], 'String');
      if (data.hasOwnProperty('deletedAt'))
        obj.deletedAt = ApiClient.convertToType(data['deletedAt'], 'String');
      if (data.hasOwnProperty('hide'))
        obj.hide = ApiClient.convertToType(data['hide'], 'Boolean');
      if (data.hasOwnProperty('id'))
        obj.id = ApiClient.convertToType(data['id'], 'String');
      if (data.hasOwnProperty('number'))
        obj._number = ApiClient.convertToType(data['number'], 'Number');
      if (data.hasOwnProperty('section'))
        obj.section = ModelsSection.constructFromObject(data['section']);
      if (data.hasOwnProperty('sectionID'))
        obj.sectionID = ApiClient.convertToType(data['sectionID'], 'String');
      if (data.hasOwnProperty('toc'))
        obj.toc = ApiClient.convertToType(data['toc'], ['String']);
      if (data.hasOwnProperty('updatedAt'))
        obj.updatedAt = ApiClient.convertToType(data['updatedAt'], 'String');
    }
    return obj;
  }
}

/**
 * @member {Array.<String>} content
 */
ModelsPage.prototype.content = undefined;

/**
 * @member {String} createdAt
 */
ModelsPage.prototype.createdAt = undefined;

/**
 * @member {String} deletedAt
 */
ModelsPage.prototype.deletedAt = undefined;

/**
 * @member {Boolean} hide
 */
ModelsPage.prototype.hide = undefined;

/**
 * @member {String} id
 */
ModelsPage.prototype.id = undefined;

/**
 * @member {Number} _number
 */
ModelsPage.prototype._number = undefined;

/**
 * @member {module:model/ModelsSection} section
 */
ModelsPage.prototype.section = undefined;

/**
 * @member {String} sectionID
 */
ModelsPage.prototype.sectionID = undefined;

/**
 * @member {Array.<String>} toc
 */
ModelsPage.prototype.toc = undefined;

/**
 * @member {String} updatedAt
 */
ModelsPage.prototype.updatedAt = undefined;

