/*
 * User API
 * This is a server to generate slides.
 *
 * OpenAPI spec version: 1.0
 * Contact: contact@solution-libre.fr
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 *
 * Swagger Codegen version: 3.0.46
 *
 * Do not edit the class manually.
 *
 */
import {ApiClient} from './ApiClient';
import {DtoCreateCourseFromGitInput} from './model/DtoCreateCourseFromGitInput';
import {DtoCreateCourseFromGitOutput} from './model/DtoCreateCourseFromGitOutput';
import {DtoCreateCourseInput} from './model/DtoCreateCourseInput';
import {DtoCreateCourseOutput} from './model/DtoCreateCourseOutput';
import {DtoCreateGroupInput} from './model/DtoCreateGroupInput';
import {DtoCreateOrganisationInput} from './model/DtoCreateOrganisationInput';
import {DtoCreatePermissionInput} from './model/DtoCreatePermissionInput';
import {DtoCreateRoleInput} from './model/DtoCreateRoleInput';
import {DtoCreateSshKeyInput} from './model/DtoCreateSshKeyInput';
import {DtoCreateUserInput} from './model/DtoCreateUserInput';
import {DtoGenerateCourseInput} from './model/DtoGenerateCourseInput';
import {DtoGenerateCourseOutput} from './model/DtoGenerateCourseOutput';
import {DtoGroupEditInput} from './model/DtoGroupEditInput';
import {DtoGroupOutput} from './model/DtoGroupOutput';
import {DtoOrganisationEditInput} from './model/DtoOrganisationEditInput';
import {DtoOrganisationOutput} from './model/DtoOrganisationOutput';
import {DtoPermissionEditInput} from './model/DtoPermissionEditInput';
import {DtoPermissionOutput} from './model/DtoPermissionOutput';
import {DtoRoleEditInput} from './model/DtoRoleEditInput';
import {DtoRoleOutput} from './model/DtoRoleOutput';
import {DtoUserEditInput} from './model/DtoUserEditInput';
import {DtoUserLoginInput} from './model/DtoUserLoginInput';
import {DtoUserLoginOutput} from './model/DtoUserLoginOutput';
import {DtoUserOutput} from './model/DtoUserOutput';
import {DtoUserRefreshTokenInput} from './model/DtoUserRefreshTokenInput';
import {DtoUserTokens} from './model/DtoUserTokens';
import {GormDeletedAt} from './model/GormDeletedAt';
import {ModelsChapter} from './model/ModelsChapter';
import {ModelsCourse} from './model/ModelsCourse';
import {ModelsFormat} from './model/ModelsFormat';
import {ModelsGroup} from './model/ModelsGroup';
import {ModelsOrganisation} from './model/ModelsOrganisation';
import {ModelsPage} from './model/ModelsPage';
import {ModelsSection} from './model/ModelsSection';
import {ModelsSshKey} from './model/ModelsSshKey';
import {ModelsUser} from './model/ModelsUser';
import {SoliFormationsSrcAuthErrorsAPIError} from './model/SoliFormationsSrcAuthErrorsAPIError';
import {SoliFormationsSrcCoursesErrorsAPIError} from './model/SoliFormationsSrcCoursesErrorsAPIError';
import {CoursesApi} from './api/CoursesApi';
import {GroupsApi} from './api/GroupsApi';
import {LoginApi} from './api/LoginApi';
import {OrganisationsApi} from './api/OrganisationsApi';
import {PermissionsApi} from './api/PermissionsApi';
import {RefreshApi} from './api/RefreshApi';
import {RolesApi} from './api/RolesApi';
import {UsersApi} from './api/UsersApi';

/**
* This_is_a_server_to_generate_slides_.<br>
* The <code>index</code> module provides access to constructors for all the classes which comprise the public API.
* <p>
* An AMD (recommended!) or CommonJS application will generally do something equivalent to the following:
* <pre>
* var UserApi = require('index'); // See note below*.
* var xxxSvc = new UserApi.XxxApi(); // Allocate the API class we're going to use.
* var yyyModel = new UserApi.Yyy(); // Construct a model instance.
* yyyModel.someProperty = 'someValue';
* ...
* var zzz = xxxSvc.doSomething(yyyModel); // Invoke the service.
* ...
* </pre>
* <em>*NOTE: For a top-level AMD script, use require(['index'], function(){...})
* and put the application logic within the callback function.</em>
* </p>
* <p>
* A non-AMD browser application (discouraged) might do something like this:
* <pre>
* var xxxSvc = new UserApi.XxxApi(); // Allocate the API class we're going to use.
* var yyy = new UserApi.Yyy(); // Construct a model instance.
* yyyModel.someProperty = 'someValue';
* ...
* var zzz = xxxSvc.doSomething(yyyModel); // Invoke the service.
* ...
* </pre>
* </p>
* @module index
* @version 1.0
*/
export {
    /**
     * The ApiClient constructor.
     * @property {module:ApiClient}
     */
    ApiClient,

    /**
     * The DtoCreateCourseFromGitInput model constructor.
     * @property {module:model/DtoCreateCourseFromGitInput}
     */
    DtoCreateCourseFromGitInput,

    /**
     * The DtoCreateCourseFromGitOutput model constructor.
     * @property {module:model/DtoCreateCourseFromGitOutput}
     */
    DtoCreateCourseFromGitOutput,

    /**
     * The DtoCreateCourseInput model constructor.
     * @property {module:model/DtoCreateCourseInput}
     */
    DtoCreateCourseInput,

    /**
     * The DtoCreateCourseOutput model constructor.
     * @property {module:model/DtoCreateCourseOutput}
     */
    DtoCreateCourseOutput,

    /**
     * The DtoCreateGroupInput model constructor.
     * @property {module:model/DtoCreateGroupInput}
     */
    DtoCreateGroupInput,

    /**
     * The DtoCreateOrganisationInput model constructor.
     * @property {module:model/DtoCreateOrganisationInput}
     */
    DtoCreateOrganisationInput,

    /**
     * The DtoCreatePermissionInput model constructor.
     * @property {module:model/DtoCreatePermissionInput}
     */
    DtoCreatePermissionInput,

    /**
     * The DtoCreateRoleInput model constructor.
     * @property {module:model/DtoCreateRoleInput}
     */
    DtoCreateRoleInput,

    /**
     * The DtoCreateSshKeyInput model constructor.
     * @property {module:model/DtoCreateSshKeyInput}
     */
    DtoCreateSshKeyInput,

    /**
     * The DtoCreateUserInput model constructor.
     * @property {module:model/DtoCreateUserInput}
     */
    DtoCreateUserInput,

    /**
     * The DtoGenerateCourseInput model constructor.
     * @property {module:model/DtoGenerateCourseInput}
     */
    DtoGenerateCourseInput,

    /**
     * The DtoGenerateCourseOutput model constructor.
     * @property {module:model/DtoGenerateCourseOutput}
     */
    DtoGenerateCourseOutput,

    /**
     * The DtoGroupEditInput model constructor.
     * @property {module:model/DtoGroupEditInput}
     */
    DtoGroupEditInput,

    /**
     * The DtoGroupOutput model constructor.
     * @property {module:model/DtoGroupOutput}
     */
    DtoGroupOutput,

    /**
     * The DtoOrganisationEditInput model constructor.
     * @property {module:model/DtoOrganisationEditInput}
     */
    DtoOrganisationEditInput,

    /**
     * The DtoOrganisationOutput model constructor.
     * @property {module:model/DtoOrganisationOutput}
     */
    DtoOrganisationOutput,

    /**
     * The DtoPermissionEditInput model constructor.
     * @property {module:model/DtoPermissionEditInput}
     */
    DtoPermissionEditInput,

    /**
     * The DtoPermissionOutput model constructor.
     * @property {module:model/DtoPermissionOutput}
     */
    DtoPermissionOutput,

    /**
     * The DtoRoleEditInput model constructor.
     * @property {module:model/DtoRoleEditInput}
     */
    DtoRoleEditInput,

    /**
     * The DtoRoleOutput model constructor.
     * @property {module:model/DtoRoleOutput}
     */
    DtoRoleOutput,

    /**
     * The DtoUserEditInput model constructor.
     * @property {module:model/DtoUserEditInput}
     */
    DtoUserEditInput,

    /**
     * The DtoUserLoginInput model constructor.
     * @property {module:model/DtoUserLoginInput}
     */
    DtoUserLoginInput,

    /**
     * The DtoUserLoginOutput model constructor.
     * @property {module:model/DtoUserLoginOutput}
     */
    DtoUserLoginOutput,

    /**
     * The DtoUserOutput model constructor.
     * @property {module:model/DtoUserOutput}
     */
    DtoUserOutput,

    /**
     * The DtoUserRefreshTokenInput model constructor.
     * @property {module:model/DtoUserRefreshTokenInput}
     */
    DtoUserRefreshTokenInput,

    /**
     * The DtoUserTokens model constructor.
     * @property {module:model/DtoUserTokens}
     */
    DtoUserTokens,

    /**
     * The GormDeletedAt model constructor.
     * @property {module:model/GormDeletedAt}
     */
    GormDeletedAt,

    /**
     * The ModelsChapter model constructor.
     * @property {module:model/ModelsChapter}
     */
    ModelsChapter,

    /**
     * The ModelsCourse model constructor.
     * @property {module:model/ModelsCourse}
     */
    ModelsCourse,

    /**
     * The ModelsFormat model constructor.
     * @property {module:model/ModelsFormat}
     */
    ModelsFormat,

    /**
     * The ModelsGroup model constructor.
     * @property {module:model/ModelsGroup}
     */
    ModelsGroup,

    /**
     * The ModelsOrganisation model constructor.
     * @property {module:model/ModelsOrganisation}
     */
    ModelsOrganisation,

    /**
     * The ModelsPage model constructor.
     * @property {module:model/ModelsPage}
     */
    ModelsPage,

    /**
     * The ModelsSection model constructor.
     * @property {module:model/ModelsSection}
     */
    ModelsSection,

    /**
     * The ModelsSshKey model constructor.
     * @property {module:model/ModelsSshKey}
     */
    ModelsSshKey,

    /**
     * The ModelsUser model constructor.
     * @property {module:model/ModelsUser}
     */
    ModelsUser,

    /**
     * The SoliFormationsSrcAuthErrorsAPIError model constructor.
     * @property {module:model/SoliFormationsSrcAuthErrorsAPIError}
     */
    SoliFormationsSrcAuthErrorsAPIError,

    /**
     * The SoliFormationsSrcCoursesErrorsAPIError model constructor.
     * @property {module:model/SoliFormationsSrcCoursesErrorsAPIError}
     */
    SoliFormationsSrcCoursesErrorsAPIError,

    /**
    * The CoursesApi service constructor.
    * @property {module:api/CoursesApi}
    */
    CoursesApi,

    /**
    * The GroupsApi service constructor.
    * @property {module:api/GroupsApi}
    */
    GroupsApi,

    /**
    * The LoginApi service constructor.
    * @property {module:api/LoginApi}
    */
    LoginApi,

    /**
    * The OrganisationsApi service constructor.
    * @property {module:api/OrganisationsApi}
    */
    OrganisationsApi,

    /**
    * The PermissionsApi service constructor.
    * @property {module:api/PermissionsApi}
    */
    PermissionsApi,

    /**
    * The RefreshApi service constructor.
    * @property {module:api/RefreshApi}
    */
    RefreshApi,

    /**
    * The RolesApi service constructor.
    * @property {module:api/RolesApi}
    */
    RolesApi,

    /**
    * The UsersApi service constructor.
    * @property {module:api/UsersApi}
    */
    UsersApi
};
